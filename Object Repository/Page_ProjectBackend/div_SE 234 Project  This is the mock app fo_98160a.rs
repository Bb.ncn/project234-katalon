<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_SE 234 Project  This is the mock app fo_98160a</name>
   <tag></tag>
   <elementGuidId>80c873bc-6f4a-45b4-953f-9075ac96b629</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//app-top-banner/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jumbotron</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
  SE 234 Project
  This is the mock app for the SE 234 project
  
  
    
      
        
          Products
          
        
        
          Carts
            2
          
        
        
      
      Logout
    
  
</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/app-root[1]/div[@class=&quot;row&quot;]/app-top-banner[@class=&quot;col-12&quot;]/div[@class=&quot;jumbotron&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//app-top-banner/div</value>
   </webElementXpaths>
</WebElementEntity>
