<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Test - Login and Logout</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>4e827407-7731-4f1b-85e2-3b0dec38ff35</testSuiteGuid>
   <testCaseLink>
      <guid>78bbaf28-fa6d-4261-8ba2-166449c2910d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test - Login and Logout/Alert - empty input</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bf5a5112-1e3c-4789-975b-1586c517b036</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test - Login and Logout/Alert - incorrect input</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cc157c71-3b3e-4a0e-8340-020f3a7312b6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test - Login and Logout/Login as admin</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>928465a8-a91a-4d31-ac69-d456e1c9a5d0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test - Login and Logout/Login as user</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d50e1d74-6862-43d4-89f5-159efa0b8fe0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test - Login and Logout/Logout</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
