<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Test - Products page</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>e016ff08-0652-4728-b97c-8ea2326d9bed</testSuiteGuid>
   <testCaseLink>
      <guid>d8a215df-e31f-416c-b3f9-fc114831c624</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test - Products page/already added button</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6035668a-6a30-4b07-9fdd-060cbd6628c0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test - Products page/Available products</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
